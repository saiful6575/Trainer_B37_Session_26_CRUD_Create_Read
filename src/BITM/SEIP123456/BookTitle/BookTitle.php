<?php
namespace App\BookTitle;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;

class BookTitle extends DB{
    public $id="";
    public $book_title="";
    public $author_name="";

    public function __construct(){
    parent:: __construct();
    if(!isset( $_SESSION)) session_start();
}

    public function setData($data=NULL){

        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }

        if(array_key_exists('book_title',$data)){
            $this->book_title = $data['book_title'];
        }

        if(array_key_exists('author_name',$data)){
            $this->author_name = $data['author_name'];
        }
    }


  public function store(){

      $arrData  = array($this->book_title,$this->author_name);

      $sql = "INSERT INTO book_title ( book_title, author_name) VALUES ( ?, ?)";

      $STH = $this->DBH->prepare($sql);


       $STH->execute($arrData);

      Message::message("<div id='msg'></div><h3 align='center'>[ BookTitle: $this->book_title ] , [ AuthorName: $this->author_name ] <br> Data Has Been Inserted Successfully!</h3></div>");


      Utility::redirect('create.php');

   }







/*

    public function store(){

          Message::message("<h3 align='center'>[ BookTitle: $this->book_title ] , [ AuthorName: $this->author_name ] <br> Data Has Been Inserted Successfully!</h3>");
          Utility::redirect('create.php');

    }
*/


    public function index($Mode="ASSOC"){
        $Mode = strtoupper($Mode);

        $STH = $this->DBH->query('SELECT * from book_title');


        if($Mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;


    }



}

?>

